package com.example.gymapp

import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.SharedPreferences
import android.net.ConnectivityManager
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import okhttp3.MediaType
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.RequestBody.Companion.toRequestBody
import org.json.JSONArray
import org.json.JSONObject
import java.lang.Exception
import java.net.HttpURLConnection
import java.net.URL


class sign_up : AppCompatActivity() {

    var client: OkHttpClient = OkHttpClient()
    val begin = System.nanoTime()
    val screenName = "sign_up"

    private lateinit var fusedLocationClient: FusedLocationProviderClient


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_up)

        val nombres = findViewById(R.id.editTextTextPersonName3) as EditText
        val apellidos = findViewById(R.id.editTextApellido) as EditText
        val email = findViewById(R.id.editTextTextEmailAddress) as EditText
        val password = findViewById(R.id.editTextTextPassword2) as EditText
        val fecha = findViewById(R.id.editTextFecha) as EditText
        val telefon = findViewById(R.id.editTextTextEmailAddress2) as EditText
        val genero = "N"

        val idStorage = ""

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)



        fun checkConnectivity() {
            val manager = this.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val activeNetwork = manager.activeNetworkInfo

            if (null == activeNetwork) {
                val dialogBuilder = AlertDialog.Builder(this)
                val intent = Intent(this, sign_up::class.java)
                // set message of alert dialog
                dialogBuilder.setMessage("Make sure that WI-FI or mobile data is turned on, then try again")
                    // if the dialog is cancelable
                    .setCancelable(false)
                    // positive button text and action
                    .setPositiveButton("Retry", DialogInterface.OnClickListener { dialog, id ->
                        recreate()
                    })
                    // negative button text and action
                    .setNegativeButton("Cancel", DialogInterface.OnClickListener { dialog, id ->
                        finish()
                    })

                // create dialog box
                val alert = dialogBuilder.create()
                // set title for alert dialog box
                alert.setTitle("No Internet Connection")
                alert.setIcon(R.mipmap.ic_launcher)
                // show alert dialog
                alert.show()
            }}
        checkConnectivity()

        fun postRequest(sUrl: String, nombres: String, password: String, email: String, apellido: String, fecha: String, genero: String, numero: String): String? {
            var result: String? = null

            try {
                val url = URL(sUrl)
                val json = JSONObject()
                val JSON: MediaType? = "application/json; charset=utf-8".toMediaTypeOrNull()


                json.put("email", email)
                json.put("phone_number", numero)
                json.put("names", nombres)
                json.put("family_names", apellido)
                json.put("gender", genero)
                json.put("born_at", fecha)
                json.put("password", password)


                val body = json.toString().toRequestBody()



                val conn = url.openConnection() as HttpURLConnection
                val request =
                    Request.Builder().addHeader("x-api-key", "zXWVuTH958OGqED%vV&BjpVE2j3IpA")
                        .addHeader("Authorization", "Bearer null")
                        .addHeader("Content-Type", "application/json").url(url).post(body).build()
                val response = client.newCall(request).execute()
                result = response.body?.string()
                val jsonArray = JSONArray(result)
            }
            catch(err: Exception){
                print("Error when executing get request: "+err.localizedMessage)
            }

            return result
        }

        val buttonClick = findViewById<Button>(R.id.button)
        buttonClick.setOnClickListener {
            val intent = Intent(this, HomeActivity::class.java)

            val nom = nombres.text.toString()
            val apellido = apellidos.text.toString()
            val correo = email.text.toString()
            val cont = password.text.toString()
            val date = fecha.text.toString()
            val gen = genero
            val num = telefon.text.toString()

            if(nom.isNotEmpty() && apellido.isNotEmpty() && correo.isNotEmpty() && cont.isNotEmpty() && date.isNotEmpty() && num.isNotEmpty() && correo.contains("@")){
                val cache:Cache =  TCache()
                cache.put("nombre",nom)
                cache.put("apellido",apellido)
                cache.put("correo",correo)
                cache.put("contrasenia",cont)
                cache.put("date",date)
                cache.put("genero",gen)
                cache.put("telefono", num)




                GlobalScope.launch {

                    val resp = postRequest(
                        BuildConfig.url + BuildConfig.urlRegiste,
                        nom,cont,correo,apellido,date,gen,num
                    )
                    print((resp))
                    if (resp != "") {
                    }

                }
                Toast.makeText(this,  "te hemos mandado un correo para validar antes de seguir" , Toast.LENGTH_LONG).show()



            }
            else{
                Toast.makeText(this,  "Datos invalidos" , Toast.LENGTH_LONG).show()

            }

        }

        val buttonClick2 = findViewById<TextView>(R.id.login)
        buttonClick2.setOnClickListener {
            val end = System.nanoTime()
            GlobalScope.launch {
                postTimeRequest(BuildConfig.url+BuildConfig.urlTime, end-begin)
            }
            val intent = Intent(this, login::class.java)
            startActivity(intent)
        }
    }

    fun postTimeRequest(sUrl: String, endTime: Long) {
        var result: String? = null
        try {
            val url = URL(sUrl)
            val JSON: MediaType? = "application/json; charset=utf-8".toMediaTypeOrNull()
            val jsonObject = JSONObject()
            jsonObject.put("screen_id", screenName)
            jsonObject.put("time", endTime)
            jsonObject.put("user_id", "UID")
            val body = jsonObject.toString().toRequestBody()
            val request =
                Request.Builder().addHeader("x-api-key", "zXWVuTH958OGqED%vV&BjpVE2j3IpA")
                    .addHeader("Authorization", "Bearer null")
                    .addHeader("Content-Type", "application/json").url(url).post(body).build()
            val response = client.newCall(request).execute()
            result = response.body?.string()
            val jsonArray = JSONArray(result)

        } catch (err: Exception) {
            print("Error when executing get request: " + err.localizedMessage)
        }
    }



}

