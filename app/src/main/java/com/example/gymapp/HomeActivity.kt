package com.example.gymapp

import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.SharedPreferences
import android.net.ConnectivityManager
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.ImageButton
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import okhttp3.MediaType
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.RequestBody.Companion.toRequestBody
import org.json.JSONArray
import org.json.JSONObject
import java.net.URL
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.net.HttpURLConnection

class HomeActivity: AppCompatActivity() {

    val begin = System.nanoTime()

    var client: OkHttpClient = OkHttpClient()

    val screenName = "home"

    val url = BuildConfig.url + BuildConfig.urlChangePw

    private  fun saveData() {

        val userName = findViewById(R.id.textView10) as EditText

        val shared: SharedPreferences = getSharedPreferences("sheredPref", Context.MODE_PRIVATE)
        val edit: SharedPreferences.Editor = shared.edit()
        edit.apply {
            putString("password", userName.text.toString().trim())
        }.apply()

        Toast.makeText(this, "Data saved", Toast.LENGTH_SHORT).show()
    }

    override fun onCreate(savedInstanceState: Bundle?) {



        super.onCreate(savedInstanceState)
        setContentView(R.layout.home)

        fun checkConnectivity() :Boolean{
            val manager = this.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val activeNetwork = manager.activeNetworkInfo

            if (null == activeNetwork) {
                val dialogBuilder = AlertDialog.Builder(this)
                val intent = Intent(this, login::class.java)
                // set message of alert dialog
                dialogBuilder.setMessage("Make sure that WI-FI or mobile data is turned on, then try again")
                    // if the dialog is cancelable
                    .setCancelable(false)
                    // positive button text and action
                    .setPositiveButton("Retry", DialogInterface.OnClickListener { dialog, id ->
                        recreate()
                    })
                    // negative button text and action
                    .setNegativeButton("Cancel", DialogInterface.OnClickListener { dialog, id ->
                        finish()
                    })

                // create dialog box
                val alert = dialogBuilder.create()
                // set title for alert dialog box
                alert.setTitle("No Internet Connection")
                alert.setIcon(R.mipmap.ic_launcher)
                // show alert dialog
                alert.show()
                return false
            }
            return true
        }

        checkConnectivity()


        fun postRequest(sUrl: String, contraseña: String?, email: String?): String? {
            var result: String? = null

            try {
                val url = URL(sUrl)
                val json = JSONObject()
                val JSON: MediaType? = "application/json; charset=utf-8".toMediaTypeOrNull()


                json.put("email", email)
                json.put("passwor", contraseña)



                val body = json.toString().toRequestBody()

                val conn = url.openConnection() as HttpURLConnection
                val request =
                    Request.Builder().addHeader("x-api-key", "zXWVuTH958OGqED%vV&BjpVE2j3IpA")
                        .addHeader("Authorization", "Bearer null")
                        .addHeader("Content-Type", "application/json").url(url).post(body).build()
                val response = client.newCall(request).execute()
                result = response.body?.string()
                val jsonArray = JSONArray(result)
            }
            catch(err: java.lang.Exception){
                print("Error when executing get request: "+err.localizedMessage)
            }

            return result
        }


        val buttosnSave = findViewById<Button>(R.id.button5)
        buttosnSave.setOnClickListener {
            val a = checkConnectivity()
            if(a) {
                saveData()
                val intent = Intent(this, HomeActivity::class.java)
                val shared: SharedPreferences =
                    getSharedPreferences("sheredPref", Context.MODE_PRIVATE)
                val savedString: String? = shared.getString("password", null)
                val user: String? = shared.getString("STRING_KEY", null)

                postRequest(url, user, savedString)
            }

        }

        val buttonClick = findViewById<ImageButton>(R.id.imageButton3)
        buttonClick.setOnClickListener {
            val end = System.nanoTime()
            GlobalScope.launch {
                postTimeRequest(BuildConfig.url+BuildConfig.urlTime, end-begin)
            }
            val intent = Intent(this, MatchActivity::class.java)
            startActivity(intent)

        }

        val buttonClick2 = findViewById<ImageButton>(R.id.imageButton4)
        buttonClick2.setOnClickListener {
            val end = System.nanoTime()
            GlobalScope.launch {
                postTimeRequest(BuildConfig.url+BuildConfig.urlTime, end-begin)
            }
            val intent = Intent(this, ProfileActivity::class.java)
            startActivity(intent)
        }

        val buttonClick3 = findViewById<ImageButton>(R.id.imageButton)
        buttonClick3.setOnClickListener {
            val end = System.nanoTime()
            GlobalScope.launch {
                postTimeRequest(BuildConfig.url+BuildConfig.urlTime, end-begin)
            }
            val intent = Intent(this, MapsActivity::class.java)
            startActivity(intent)
        }


    }
    fun postTimeRequest(sUrl: String, endTime: Long) {
        var result: String? = null
        try {
            val url = URL(sUrl)
            val JSON: MediaType? = "application/json; charset=utf-8".toMediaTypeOrNull()
            val jsonObject = JSONObject()
            jsonObject.put("screen_id", screenName)
            jsonObject.put("time", endTime)
            jsonObject.put("user_id", "UID")
            val body = jsonObject.toString().toRequestBody()
            val request =
                Request.Builder().addHeader("x-api-key", "zXWVuTH958OGqED%vV&BjpVE2j3IpA")
                    .addHeader("Authorization", "Bearer null")
                    .addHeader("Content-Type", "application/json").url(url).post(body).build()
            val response = client.newCall(request).execute()
            result = response.body?.string()
            val jsonArray = JSONArray(result)

        } catch (err: Exception) {
            print("Error when executing get request: " + err.localizedMessage)
        }
    }
}