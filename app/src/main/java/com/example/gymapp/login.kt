package com.example.gymapp

import android.content.Context
import android.content.DialogInterface
import android.net.ConnectivityManager
import android.widget.*
import androidx.appcompat.app.AlertDialog
import kotlinx.coroutines.launch
import org.json.JSONObject
import java.net.HttpURLConnection
import java.net.URL
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import kotlinx.coroutines.GlobalScope
import okhttp3.MediaType
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.RequestBody.Companion.toRequestBody
import org.json.JSONArray
import java.lang.Exception
import android.content.SharedPreferences


class login : AppCompatActivity() {

    val begin = System.nanoTime()

    val screenName = "log_in"

    var client: OkHttpClient = OkHttpClient()

    private  fun saveDataContrasenia() {
        val userName = findViewById(R.id.userName) as EditText


        val shared: SharedPreferences = getSharedPreferences("sheredPref", Context.MODE_PRIVATE)
        val edit: SharedPreferences.Editor = shared.edit()
        edit.apply {
            putString("STRING_KEY", userName.text.toString())
        }.apply()

        Toast.makeText(this, "Data saved", Toast.LENGTH_SHORT).show()
    }

    private  fun saveData() {
        val userName = findViewById(R.id.userName) as EditText


        val shared: SharedPreferences = getSharedPreferences("sheredPref", Context.MODE_PRIVATE)
        val edit: SharedPreferences.Editor = shared.edit()
        edit.apply {
            putString("STRING_KEY", userName.text.toString())
        }.apply()

        Toast.makeText(this, "Data saved", Toast.LENGTH_SHORT).show()
    }

    private fun loadData(){
        val shared: SharedPreferences = getSharedPreferences("sheredPref", Context.MODE_PRIVATE)
        val savedString : String? = shared.getString("STRING_KEY", null)
        val userName = findViewById(R.id.userName) as EditText
        userName.setText(savedString)
    }

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        loadData()

        val buttonLogin = findViewById(R.id.buttonLogin) as Button
        val userName = findViewById(R.id.userName) as EditText
        val password = findViewById(R.id.password) as EditText

        fun checkConnectivity() {
            val manager = this.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val activeNetwork = manager.activeNetworkInfo

            if (null == activeNetwork) {
                val dialogBuilder = AlertDialog.Builder(this)
                val intent = Intent(this, login::class.java)
                // set message of alert dialog
                dialogBuilder.setMessage("Make sure that WI-FI or mobile data is turned on, then try again")
                    // if the dialog is cancelable
                    .setCancelable(false)
                    // positive button text and action
                    .setPositiveButton("Retry", DialogInterface.OnClickListener { dialog, id ->
                        recreate()
                    })
                    // negative button text and action
                    .setNegativeButton("Cancel", DialogInterface.OnClickListener { dialog, id ->
                        finish()
                    })

                // create dialog box
                val alert = dialogBuilder.create()
                // set title for alert dialog box
                alert.setTitle("No Internet Connection")
                alert.setIcon(R.mipmap.ic_launcher)
                // show alert dialog
                alert.show()
            }}
            checkConnectivity()


        fun postRequest(sUrl: String, userName: String, password: String): String? {
            var result: String? = null

            try {
                val url = URL(sUrl)
                val json = JSONObject()
                val JSON: MediaType? = "application/json; charset=utf-8".toMediaTypeOrNull()


                json.put("email", userName )
                json.put("password", password )

                val body = json.toString().toRequestBody()

                val conn = url.openConnection() as HttpURLConnection
                val request =
                    Request.Builder().addHeader("x-api-key", "zXWVuTH958OGqED%vV&BjpVE2j3IpA")
                        .addHeader("Authorization", "Bearer null")
                        .addHeader("Content-Type", "application/json").url(url).post(body).build()
                val response = client.newCall(request).execute()
                result = response.body?.string()
                val jsonArray = JSONArray(result)
            }
            catch(err:Exception){
                print("Error when executing get request: "+err.localizedMessage)
            }

            return result
        }

        val per = Persona("andres","Castillo", "1234","a@hotmail.com",3104567)

        buttonLogin.setOnClickListener {
            if (userName.text.trim().isNotEmpty() && password.text.trim().isNotEmpty() && userName.text.trim().contains("@")) {
                val name = userName.text.toString()
                val contra = password.text.toString()
                val intent = Intent(this, HomeActivity::class.java)
                val shared: SharedPreferences = getSharedPreferences("sheredPref", Context.MODE_PRIVATE)
                val savedString : String? = shared.getString("password", null)
                val user : String? = shared.getString("STRING_KEY", null)
                if( (savedString == null || savedString.toString().trim() == password.text.toString().trim()) && (user == null || user.toString().trim() == name ) ) {

                    GlobalScope.launch {
                        val resp = postRequest(BuildConfig.url + BuildConfig.urlUser, name, contra)
                        print(resp)
                        if (resp != "") {
                            startActivity(intent)
                        }
                    }

                    }
                else{
                    val shared: SharedPreferences = getSharedPreferences("sheredPref", Context.MODE_PRIVATE)
                    val savedString : String? = shared.getString("password", null)
                    Toast.makeText(this,  "Usuario o Contraseña invalida " , Toast.LENGTH_LONG).show()

                }
                }
             else {
                Toast.makeText(this,  "Datos invalidos", Toast.LENGTH_LONG).show()
            }
        }

        val buttonClick2 = findViewById<TextView>(R.id.signup)
        buttonClick2.setOnClickListener {
            val end = System.nanoTime()
            GlobalScope.launch {
                postTimeRequest(BuildConfig.url+BuildConfig.urlTime, end-begin)
            }
            val intent = Intent(this, sign_up::class.java)
            startActivity(intent)
        }
    }

    fun postTimeRequest(sUrl: String, endTime: Long) {
        var result: String? = null
        try {
            val url = URL(sUrl)
            val JSON: MediaType? = "application/json; charset=utf-8".toMediaTypeOrNull()
            val jsonObject = JSONObject()
            jsonObject.put("screen_id", screenName)
            jsonObject.put("time", endTime)
            jsonObject.put("user_id", "UID")
            val body = jsonObject.toString().toRequestBody()
            val request =
                Request.Builder().addHeader("x-api-key", "zXWVuTH958OGqED%vV&BjpVE2j3IpA")
                    .addHeader("Authorization", "Bearer null")
                    .addHeader("Content-Type", "application/json").url(url).post(body).build()
            val response = client.newCall(request).execute()
            result = response.body?.string()
            val jsonArray = JSONArray(result)

        } catch (err: Exception) {
            print("Error when executing get request: " + err.localizedMessage)
        }
    }
}





data class Persona (
    val name: String,
    val nameParent: String,
    val password: String,
    val email: String,
    val number: Int
)
