package com.example.gymapp

import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.net.ConnectivityManager
import android.os.Bundle
import android.widget.Button
import android.widget.ImageButton
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity

class IntroActivity1: AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.intro1)


        fun checkConnectivity() {
            val manager = this.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val activeNetwork = manager.activeNetworkInfo

            if (null == activeNetwork) {
                val dialogBuilder = AlertDialog.Builder(this)
                val intent = Intent(this, login::class.java)
                // set message of alert dialog
                dialogBuilder.setMessage("Make sure that WI-FI or mobile data is turned on, then try again")
                    // if the dialog is cancelable
                    .setCancelable(false)
                    // positive button text and action
                    .setPositiveButton("Retry", DialogInterface.OnClickListener { dialog, id ->
                        recreate()
                    })
                    // negative button text and action
                    .setNegativeButton("Cancel", DialogInterface.OnClickListener { dialog, id ->
                        finish()
                    })

                // create dialog box
                val alert = dialogBuilder.create()
                // set title for alert dialog box
                alert.setTitle("No Internet Connection")
                alert.setIcon(R.mipmap.ic_launcher)
                // show alert dialog
                alert.show()
            }}
        checkConnectivity()

        val buttonClick = findViewById<Button>(R.id.button2)
        buttonClick.setOnClickListener {
            val intent = Intent(this, IntroActivity2::class.java)
            startActivity(intent)
        }


        }
    }
