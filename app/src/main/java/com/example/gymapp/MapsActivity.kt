package com.example.gymapp


import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.net.ConnectivityManager
import android.os.Bundle
import android.widget.ImageButton
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import okhttp3.MediaType
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.RequestBody.Companion.toRequestBody
import org.json.JSONArray
import org.json.JSONObject
import java.net.URL
import java.util.concurrent.ConcurrentHashMap

class MapsActivity : AppCompatActivity(), OnMapReadyCallback {

    var client: OkHttpClient = OkHttpClient()
    var gyms: MutableList<LatLng> =  ArrayList()
    var names: MutableList<String> =  ArrayList()

    val begin = System.nanoTime()
    val screenName = "maps"

    private lateinit var fusedLocationClient: FusedLocationProviderClient

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_maps)

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)


        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)


        val buttonClick = findViewById<ImageButton>(R.id.imageButton2)
        buttonClick.setOnClickListener {
            val end = System.nanoTime()
            GlobalScope.launch {
                postTimeRequest(BuildConfig.url+BuildConfig.urlTime, end-begin)
            }
            val intent = Intent(this, HomeActivity::class.java)
            startActivity(intent)
        }

        val buttonClick2 = findViewById<ImageButton>(R.id.imageButton4)
        buttonClick2.setOnClickListener {
            val end = System.nanoTime()
            GlobalScope.launch {
                postTimeRequest(BuildConfig.url+BuildConfig.urlTime, end-begin)
            }
            val intent = Intent(this, ProfileActivity::class.java)
            startActivity(intent)
        }

        val buttonClick3 = findViewById<ImageButton>(R.id.imageButton3)
        buttonClick3.setOnClickListener {
            val end = System.nanoTime()
            GlobalScope.launch {
                postTimeRequest(BuildConfig.url+BuildConfig.urlTime, end-begin)
            }
            val intent = Intent(this, MatchActivity::class.java)
            startActivity(intent)
        }



        fun checkConnectivity() {
            val manager = this.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val activeNetwork = manager.activeNetworkInfo

            if (null == activeNetwork) {
                val dialogBuilder = androidx.appcompat.app.AlertDialog.Builder(this)
                val intent = Intent(this, login::class.java)
                // set message of alert dialog
                dialogBuilder.setMessage("Make sure that WI-FI or mobile data is turned on, then try again")
                    // if the dialog is cancelable
                    .setCancelable(false)
                    // positive button text and action
                    .setPositiveButton("Retry", DialogInterface.OnClickListener { dialog, id ->
                        recreate()
                    })
                    // negative button text and action
                    .setNegativeButton("Cancel", DialogInterface.OnClickListener { dialog, id ->
                        finish()
                    })

                // create dialog box
                val alert = dialogBuilder.create()
                // set title for alert dialog box
                alert.setTitle("No Internet Connection")
                alert.setIcon(R.mipmap.ic_launcher)
                // show alert dialog
                alert.show()
            }
        }
        checkConnectivity()



        GlobalScope.launch {
            postRequest(BuildConfig.urlgetgyms)
        }


    }


    fun postRequest(sUrl: String): MutableList<LatLng> {
        var result: String? = null
        try {
            val url = URL(sUrl)
            val JSON: MediaType? = "application/json; charset=utf-8".toMediaTypeOrNull()
            val jsonObject = JSONObject()
            jsonObject.put("latitude", 4.602607)
            jsonObject.put("longitude", -74.065307)
            val body = jsonObject.toString().toRequestBody()
            val request =
                Request.Builder().addHeader("x-api-key", "zXWVuTH958OGqED%vV&BjpVE2j3IpA")
                    .addHeader("Authorization", "Bearer null")
                    .addHeader("Content-Type", "application/json").url(url).post(body).build()
            val response = client.newCall(request).execute()
            result = response.body?.string()
            val jsonArray = JSONArray(result)

            for (g in 0 until jsonArray.length()) {

                val lat = jsonArray.getJSONObject(g).getString("latitude")
                val long = jsonArray.getJSONObject(g).getString("longitude")
                val name = jsonArray.getJSONObject(g).getString("name")
                val gym = LatLng(lat.toDouble(), long.toDouble())
                gyms.add(gym)
                names.add("$name\n \n")

            }

        } catch (err: Exception) {
            print("Error when executing get request: " + err.localizedMessage)
        }

        return gyms
    }

    override fun onMapReady(googleMap: GoogleMap) {
        val bog = LatLng(4.602607, -74.065307)

        Thread.sleep(3_000)

            for (i in gyms) {
                val mapFragment = supportFragmentManager.findFragmentById(
                    R.id.map
                ) as? SupportMapFragment
                mapFragment?.getMapAsync { googleMap ->
                    googleMap.moveCamera(CameraUpdateFactory.newLatLng(i))

                    googleMap.addMarker(MarkerOptions().position(i))

                    val cache:Cache =  TCache()
                    cache.put(i.toString(),i.toString())


                }
            }
            googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(bog, 300f))

        findViewById<TextView>(R.id.textView11).setText(names.toString()
            .replace(",", "")
            .replace("[", "")
            .replace("]", "")
        )
    }

    fun postTimeRequest(sUrl: String, endTime: Long) {
        var result: String? = null
        try {
            val url = URL(sUrl)
            val JSON: MediaType? = "application/json; charset=utf-8".toMediaTypeOrNull()
            val jsonObject = JSONObject()
            jsonObject.put("screen_id", screenName)
            jsonObject.put("time", endTime)
            jsonObject.put("user_id", "UID")
            val body = jsonObject.toString().toRequestBody()
            val request =
                Request.Builder().addHeader("x-api-key", "zXWVuTH958OGqED%vV&BjpVE2j3IpA")
                    .addHeader("Authorization", "Bearer null")
                    .addHeader("Content-Type", "application/json").url(url).post(body).build()
            val response = client.newCall(request).execute()
            result = response.body?.string()
            val jsonArray = JSONArray(result)

        } catch (err: java.lang.Exception) {
            print("Error when executing get request: " + err.localizedMessage)
        }
    }
}


interface Cache {
    fun get(key: String): String?
    fun put(key: String, value: String)
}

class TCache() : Cache {
    private var cacheTimeValidityInMillis: Long = 0
    private val hashMap = ConcurrentHashMap<String, TimedEntry>()

    override fun get(key: String): String? {
        val timedEntry = hashMap[key]
        if (timedEntry == null || timedEntry.isExpired()) {

            return null
        }

        return timedEntry.value
    }

    override fun put(key: String, value: String) {
        hashMap[key] = TimedEntry(value, cacheTimeValidityInMillis)
    }

    data class TimedEntry(val value: String, val maxDurationInMillis: Long) {
        private val creationTime: Long = now()

        fun isExpired() = (now() - creationTime) > maxDurationInMillis

        private fun now() = System.currentTimeMillis()
    }
}